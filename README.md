Web Application Development
The next phase is creating the project. Follow the list of general requirements below to build your web application:

Create HTML markup with semantic tags (<header>, <footer>, <article>, etc.). If you don't remember how to create semantic markup, please return to Accessibility fundamentals and HTML semantics.
Use the BEM methodology to name classes. If you need to refresh your memory on BEM, look through the BEM Methodology Documentation
Use Flexbox or/and Grid.
Use Sass to create styles (mixins, variables, and extend functionality are required).
The top block is an image slider (or a video with autoplay when the page loads) and the main menu of the site. Menu items are arranged with horizontal indentation and should be separated by a vertical line. The site's main menu must have a submenu. Create a top menu for desktop view using the <nav>, <ul>, <li>, and <a>
The main block of the website should have "breadcrumbs" on top. Under them is a block of content. When resizing the browser window, this block of content should be stretched over the entire space available to it, minus the left menu (if any), headers, and breadcrumbs. When scrolling through a block of content, the remaining blocks must be stationary.
The web application page should be correctly displayed on different devices in all browsers (the latest versions of all popular browsers, without Internet Explorer support). If necessary, menus and blocks can be displayed on different devices in different ways. A horizontal scroll bar should never appear when resizing a browser window.
All data on the website should be loaded via API calls.
One of the pages of your web application should contain a gallery of 300 x 300 px pictures. Provide an option for when the original images are different sizes.
Use client pagination on one of the pages: The data must be displayed page by page, with a maximum of 10 elements per page, and you must provide user navigation to navigate through the pages.
The data page should be able to sort and filter.
On the "Contacts" page, there should be a map indicating the location.
Provide the ability to search or filter products or other information (depending on the option assigned).
You can use any images or videos.
The code must be organized so that it can be reused.
Develop a mechanism for handling errors.

Points	Requirements
1	HTML markup is created with semantic tags (<header>, <footer>, <article>, etc.).
2	A top menu was created for desktop view using the <nav>, <ul>, <li>, and <a> tags.
3	The site's main menu contains a submenu.
3	When resizing the browser window, the content block stretches over the entire space available to it, minus the left menu (if any), headers, and breadcrumbs.
3	The web application page is displayed correctly on different devices in all browsers.
3	The web application page is displayed correctly for all screen resolutions.
5	The BEM methodology is used.
5	The top block contains an image slider (or a video with autoplay when the page loads).
5	On the "Contacts" page, there is a map indicating the location.
6	No horizontal scroll bar appears when resizing a browser window.
6	On one of the pages of the block of content is a gallery of 300 x 300 px pictures. Ann option is provided for when the original images are different sizes.
6	All data on the website loads via API calls.
6	The search function can be performed on pages (filter products or other information (depending on the option assigned).
6	The code is organized so that it can be reused.
6	A mechanism is developed for handling errors.
7	Client pagination is used on one of the pages: Data must be displayed page by page, with a maximum of 10 elements per page, and user navigation must be provided to navigate through the pages.
7	The data page can sort and filter.